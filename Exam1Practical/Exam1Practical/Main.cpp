
// Exam 1 Practical
// Paul Haller

#include <iostream>
#include <conio.h>

using namespace std;

void Square(float &number);
void Cube(float &number);

int main()
{
	char restart = 'n';
	do
	{
		float number;
		int select;
		cout << "Enter a number\n";
		cin >>  number;

		cout << "Select either \"2\" to square the number or \"3\" to cube the number.\n";
		while (cin >> select && (select != 2 && select != 3))
		{
			cout << "Error: Please select either a \"2\" or a \"3\"\n";
		}

		if (select == 2)
			Square(number);
		else
			Cube(number);

		cout << "The total is: " << number;

		cout << "\nWould you like to perform another calculation? (select \"Y\" to restart or anything else to exit)\n";

		cin >> restart;

		if (restart != 'y')
		{
			cout << "Press any key to exit";
			(void)_getch();
			return 0;
		}

	} while (restart == 'y');

}

void Square(float& number)
{
	number *= number;
}

void Cube(float& number)
{
	number = number * number * number;
}